require(["N/search", "N/currentRecord"], function (s, cr) {

    /**
     * An example of N/search#lookupFields to retrieve a single field from a related record
     */
    (function () {

        var record = cr.get();

        // Get the Sales Rep record ID
        var repId = record.getValue({
            "fieldId": "salesrep"
        });

        // Get the name of the Sales Rep
        var repName = record.getText({
            "fieldId": "salesrep"
        });

        // Retrieve the email address from the associated Sales Rep
        var repData = s.lookupFields({
            "type": "employee",
            "id": repId,
            "columns": ["email"]
        });

        console.log(repData);
        console.log(repName + "'s email address is " + repData.email);
    })();
});
