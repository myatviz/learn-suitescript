// Removing the first line item...

// ... if we're in the context of a record
nlapiRemoveLineItem("item", 1);

// ... or if we have a reference to the record (rec)
rec.removeLineItem("item", 1);

// Removing the last line item...

// ... if we're in the context of a record
nlapiRemoveLineItem("item", nlapiGetLineItemCount("item"));

// ... or if we have a reference to the record (rec)
rec.removeLineItem("item", rec.getLineItemCount("item"));