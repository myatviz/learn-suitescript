// Adding a line item to the end of a sublist in Standard Mode...

// ... if the record is in context:
var nextIndex = nlapiGetLineItemCount("item") + 1;
nlapiSetLineItemValue("item", "item", nextIndex, 456);
nlapiSetLineItemValue("item", "quantity", nextIndex, 10);

// ... or if we have a reference to the record (rec):
var nextIndex = rec.getLineItemCount("item") + 1;
rec.setLineItemValue("item", "item", nextIndex, 456);
rec.setLineItemValue("item", "quantity", nextIndex, 10);